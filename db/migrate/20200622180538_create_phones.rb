class CreatePhones < ActiveRecord::Migration[6.0]
  def change
    create_table :phones do |t|
      t.references :user, null: false, foreign_key: true
      t.string :name
      t.string :phone_number

      t.timestamps
    end
  end
end
