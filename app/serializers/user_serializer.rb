class UserSerializer < ApplicationSerializer
  attributes :id, :first_name, :last_name, :errors
  has_many :phones

  def errors
    object.errors.full_messages
  end
end
