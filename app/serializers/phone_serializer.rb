class PhoneSerializer < ApplicationSerializer
  attributes :id, :name, :phone_number
  has_one :user
end
