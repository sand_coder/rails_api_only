class Api::V1::UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]

  # GET /users
  def index
    @users = User.all

    render json: @users, each_serializer: UserSerializer
  end

  # GET /users/1
  def show
    render json: @user, serializer: UserSerializer
  end

  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created, location: [:api, @user], serializer: UserSerializer
    else
      render json: @user, status: :unprocessable_entity, location: [:api, @user]
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      render json: @user, status: :ok, location: [:api, @user], serializer: UserSerializer
    else
      render json: @user, status: :unprocessable_entity, location: [:api, @user]
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:first_name, :last_name, :email,:password, :password_confirmation)
    end
end
