Rails.application.routes.draw do
  devise_for :users
  
  namespace :api, defaults: { format: :json } do
    scope module: :v1, constraints: ApiConstraint.new(version: 1, default: true) do
      resources :users do
        resources :phones
      end
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
